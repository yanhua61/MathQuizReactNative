import React from 'react';
import { StyleSheet, Text } from 'react-native';

export default class Question extends React.Component {
    
    render() {
        return (
            <Text style = {styles}> {this.props.number1} + {this.props.number2} </Text>
        );   
    }
}

const styles = {
    fontSize: 70,
    fontWeight: 'bold'
};