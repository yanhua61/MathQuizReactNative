import React from 'react';
import { StyleSheet, View, Text, TouchableNativeFeedback } from 'react-native';

export default class AnswerButton extends React.Component {
    render() {
        return(
            <TouchableNativeFeedback onPress = {() => this.props.onTouch(this.props.number)}>
                <View style = {styles.button} >
                    <Text style = {styles.text} > {this.props.number} </Text>
                </View>
            </TouchableNativeFeedback>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        width: 100,
        height: 70,
        backgroundColor: '#23E835',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 5,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 35,
        fontWeight: 'bold'
    }
});