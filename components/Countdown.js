import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default class Countdown extends React.Component {
    state = {
        timeRemaining: 60
    }

    componentDidMount() {
        setInterval(
            () => this.updateTime(),
            100
        );
    }

    updateTime() {
        if (this.state.timeRemaining === 0) {
            this.props.onTimeOut();
            this.setState({timeRemaining: 60});
        } else {
            this.setState((prevState) => ({timeRemaining: prevState.timeRemaining - 1}));
        }
    }

    render() {
        return(
            <Text style = {styles.text} > {this.state.timeRemaining} </Text>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        flex: 1,
        fontSize: 25
    }
});