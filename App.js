import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Game from './screens/Game';
import { StackNavigator } from 'react-navigation';

export default class App extends React.Component {

  render() {
    return(
      <Navigator />
    );
    
  }
}

const Navigator = StackNavigator({
  Game: {
    screen: Game
  }
},
{
  initialRouteName: 'Game',
  headerMode: 'none'
}
);



