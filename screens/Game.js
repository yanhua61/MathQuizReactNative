import React from 'react';
import { View, StyleSheet } from 'react-native';
import Question from '../components/Question';
import Countdown from '../components/Countdown';
import AnswerButton from '../components/AnswerButton';

export default class Game extends React.Component {
    constructor(props) {
        super(props);
    }
    

    componentWillMount() {
        this.newQuestion();
    }

    render() {
        const { answer, wrongAnswer1, wrongAnswer2, wrongAnswer3 } = this.state;
        const answerList = [wrongAnswer1, wrongAnswer2, wrongAnswer3];
        const random = Math.floor(Math.random() * 3);
        answerList.splice(random, 0, answer);

        return (
            <View style = {styles.container}>
                <View style = {styles.topBar}>
                    <Countdown onTimeOut = {this.newQuestion.bind(this)} />
                </View>
                <View style = {styles.question}> 
                    <Question number1 = {this.state.number1} number2 = {this.state.number2} />
                </View>
                <View style = {styles.buttonRow}>
                    <AnswerButton number = {answerList.shift()} onTouch = {this.checkAnswer.bind(this)} />
                    <AnswerButton number = {answerList.pop()} onTouch = {this.checkAnswer.bind(this)}/>
                </View>
                <View style = {styles.buttonRow}>
                    <AnswerButton number = {answerList.pop()} onTouch = {this.checkAnswer.bind(this)}/>
                    <AnswerButton number = {answerList.pop()} onTouch = {this.checkAnswer.bind(this)}/>
                </View>
            </View>
        );
    }

    checkAnswer(answer) {
        if (answer == this.state.answer) {
            this.newQuestion();
        }
    }

    newQuestion() {
        const number1 = this.randomNumber(1, 10);
        const number2 = this.randomNumber(1, 10);
        const answer = number1 + number2;
        this.setState({ answer, number1, number2 }, () => this.generateWrongAnswers(1, 10));
    }

    generateWrongAnswers(min, max) {
        const { answer, wrongAnswer1, wrongAnswer2, wrongAnswer3 } = this.state;
        let wrongAnswers = [];

        while (wrongAnswers.length < 3) {
            let wrongAnswer = Math.floor(Math.random() * (max - min));
            if (!wrongAnswers.includes(wrongAnswer) && wrongAnswer != answer) {
                wrongAnswers.push(wrongAnswer);
            }
        }

        this.setState({
            wrongAnswer1: wrongAnswers[0],
            wrongAnswer2: wrongAnswers[1],
            wrongAnswer3: wrongAnswers[2]
        });
    }


    randomNumber(min, max) {
        return(Math.floor(Math.random() * (max - min)) + min);
    }


}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#45CCFE',
      flexDirection: 'column',
      alignItems: 'center'
    },
    topBar: {
        flex: 1,
        paddingTop: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    question: {
        flex: 3
    },
    buttonRow: {
        flex: 2,
        flexDirection: 'row',
    }
});
  